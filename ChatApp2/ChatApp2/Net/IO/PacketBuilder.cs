﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ChatClient.Net.IO
{
    class PacketBuilder
    {
        MemoryStream memStream;

        public PacketBuilder()
        {
            memStream = new MemoryStream();
        }

        public void WriteOpCode(byte opCode)
        {
            memStream.WriteByte(opCode);
        }

        public void WriteMessage(string msg)
        {
            var msgLength = msg.Length;
            memStream.Write(BitConverter.GetBytes(msgLength));
            memStream.Write(Encoding.ASCII.GetBytes(msg));
        }

        public byte[] GetPacketByte()
        {
            return memStream.ToArray();
        }
    }
}
