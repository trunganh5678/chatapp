﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ChatClient.MVVM.Core;
using ChatClient.MVVM.Model;
using ChatClient.Net;

namespace ChatClient.MVVM.ViewModel
{
    class MainViewModel
    {
        public ObservableCollection<UserModel> Users { get; set; }
        public ObservableCollection<string> Messages { get; set; }
        public RelayComand ConnectToServerCommand { get; set; }
        public RelayComand SendMessageCommand { get; set; }

        public string Username { get; set; }
        public string Message { get; set; }

        private Server server;
        public MainViewModel()
        {
            Users = new ObservableCollection<UserModel>();
            Messages = new ObservableCollection<string>();

            server = new Server();
            server.ConnectedEvent += UserConnected;
            server.MsgReceivedEvent += MessageReceived;
            server.UserDisconnectedEvent += RemoveUser;

            ConnectToServerCommand = new RelayComand(o => server.ConnectToServer(Username), o => !string.IsNullOrEmpty(Username));
            SendMessageCommand = new RelayComand(o => server.SendMessageToServer(Message), o => !string.IsNullOrEmpty(Message));
        }

        void UserConnected()
        {
            var user = new UserModel
            {
                Username = server.packetReader.ReadMessage(),
                UID = server.packetReader.ReadMessage()
            };

            if (!Users.Any(x => x.UID == user.UID))
            {
                Application.Current.Dispatcher.Invoke(() => Users.Add(user));
            }
        }

        void MessageReceived()
        {
            var msg = server.packetReader.ReadMessage();
            Application.Current.Dispatcher.Invoke(() => Messages.Add(msg));
        }

        void RemoveUser()
        {
            var uid = server.packetReader.ReadMessage();
            var user = Users.Where(x => x.UID == uid).FirstOrDefault();
            Application.Current.Dispatcher.Invoke(() => Users.Remove(user));
        }
    }
}
