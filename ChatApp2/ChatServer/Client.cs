﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using ChatServer.Net.IO;

namespace ChatServer
{
    class Client
    {
        public string Username { get; set; }
        public Guid UID { get; set; }
        public TcpClient clientSocket { get; set; }

        PacketReader packetReader;
        public Client(TcpClient client)
        {
            clientSocket = client;
            UID = Guid.NewGuid();
            packetReader = new PacketReader(clientSocket.GetStream());

            var opcode = packetReader.ReadByte();
            Username = packetReader.ReadMessage();

            Console.WriteLine($"[{DateTime.Now}]: Client has connected with Username: {Username}");

            Task.Run(() => Process());
        }

        void Process()
        {
            while (true)
            {
                try
                {
                    var opcode = packetReader.ReadByte();
                    switch(opcode)
                    {
                        case 5:
                            var msg = packetReader.ReadMessage();
                            Console.WriteLine($"({DateTime.Now}): Message recived : {msg}");
                            Program.BroadcastMessage($"({DateTime.Now})  [{Username}]: {msg}");
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine($"{UID}: Disconnected.");
                    Program.BroadcastDisconnect(UID.ToString());
                    clientSocket.Close();
                    break;
                }
            }
        }
    }
}
